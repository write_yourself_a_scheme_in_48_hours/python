import sys

match sys.argv:
    case [a0]: print(a0)
    case [_, a1]: print(a1)
    case _: print(sys.argv)
